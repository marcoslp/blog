<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;

$factory->define(Category::class, function (Faker $faker) {
    $title = $faker->sentence(4);
    $slug = Str::slug($title);
    return [
        'name' => $title,
        'slug' => $slug,
        'body' => $faker->text(500),
    ];
});
