@extends('layouts.app')

@section('content')
<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <h1>{{ $post->name}}</h1>
        <div class="card card-default">
            <div class="card-heading">
                Categoria
            <a href="{{ route('category',$post->category->slug)}}">{{$post->category->name}}</a>
            </div>
            <div class="card-body">
                @if ($post->file)
                    <img src="{{$post->file}}" alt="Imagen del post" class="img-fluid">
                @endif
                {{ $post->extracto}}
            <hr>
            {!!$post->body!!}
            <hr>
            Etiquetas
            @foreach ($post->tags as $tag)
                <a href="{{ route('tag',$tag->slug)}}">
                    {{$tag->name}}
                </a>
            @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
